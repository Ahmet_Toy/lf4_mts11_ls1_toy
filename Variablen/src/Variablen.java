import java.util.*;


public class Variablen {

	public static void main(String[] args) {
		
		/** Variablen.java
	    Ergaenzen Sie nach jedem Kommentar jeweils den Quellcode.
	    @author
	    @version

	    /* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
	          Vereinbaren Sie eine geeignete Variable */
		int zaehlerProgrammdurchlaeufe;

	    /* 2. Weisen Sie dem Zaehler den Wert 25 zu
	          und geben Sie ihn auf dem Bildschirm aus.*/
		//zaehlerProgrammdurchlaeufe = 25;
		zaehlerProgrammdurchlaeufe=25;
		System.out.println("Programmdurchläufe: " + zaehlerProgrammdurchlaeufe);

	    /* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
	          eines Programms ausgewaehlt werden.
	          Vereinbaren Sie eine geeignete Variable */
		char menueAuswahl;

	    /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
	          und geben Sie ihn auf dem Bildschirm aus.*/
		menueAuswahl = 'C';
		System.out.println("Auswahl Menü: " + menueAuswahl);

	    /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
	          notwendig.
	          Vereinbaren Sie eine geeignete Variable */
		long berechnung;

	    /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
	          und geben Sie sie auf dem Bildschirm aus.*/
		berechnung = 1079252820;
		System.out.println("Lichtgeschwindigkeit:" + berechnung + " km/h");

	    /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
	          soll die Anzahl der Mitglieder erfasst werden.
	          Vereinbaren Sie eine geeignete Variable und initialisieren sie
	          diese sinnvoll.*/
		
		List<String> mitglieder = new ArrayList<String>();
		mitglieder.add("Person 1");
		
		int size = mitglieder.size();

	    /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/
		System.out.println("Anzahl der Vereinsmitglieder: " + size);

	    /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
	          Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
	          dem Bildschirm aus.*/
		System.out.println("Elektrische Elementarladung = " + Math.pow(1.602*10,-19));
		

	    /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
	          Vereinbaren Sie eine geeignete Variable. */
		boolean zahlungErhalten;

	    /*11. Die Zahlung ist erfolgt.
	          Weisen Sie der Variable den entsprechenden Wert zu
	          und geben Sie die Variable auf dem Bildschirm aus.*/
		zahlungErhalten = true;
		System.out.println("Eingang der Zahlung: " + zahlungErhalten);

	}//main
} // Variablen


