
public class Argumente {

	public static void main(String[] args) {

		// Aufgabe 1:

		ausgabe(1, "Mana");
		ausgabe(2, "Elise");
		ausgabe(3, "Johanna");
		ausgabe(4, "Felizitas");
		ausgabe(5, "Karla");
		System.out.println(vergleichen(1, 2));
		System.out.println(vergleichen(1, 5));
		System.out.println(vergleichen(3, 4));

		System.out.println(wuerfel(2.0));
		System.out.println(quader(3.0, 2.5, 4.0));
		System.out.println(pyramide(2.0, 6.0, 4.0));
		System.out.printf("%2.2f", kugel(2.5));
	}

	public static void ausgabe(int zahl, String name) {
		System.out.println(zahl + ": " + name);
	}

	public static boolean vergleichen(int arg1, int arg2) {
		return (arg1 + 8) < (arg2 * 3);
	}

	// Aufgabe 2:

	double produkt = multiplizieren(2.36, 7.87);

	public static double multiplizieren(double zahl1, double zahl2) {
		double produkt = zahl1 * zahl2;
		return produkt;
	}

	// Aufgabe 3:

	public static double wuerfel(double a) {
		double wuerfelVolumen = a * a * a;
		return wuerfelVolumen;
	}

	public static double quader(double a, double b, double c) {
		double quaderVolumen = a * b * c;
		return quaderVolumen;
	}

	public static double pyramide(double a1, double a2, double h) {
		double pyramidenVolumen = a1 * a2 * h / 3;
		return pyramidenVolumen;
	}

	public static double kugel(double r) {
		double kugelVolumen = ((4 / 3) * Math.PI * r * r * r);
		return kugelVolumen;
	}

}
