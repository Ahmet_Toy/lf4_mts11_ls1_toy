import java.text.DecimalFormat;

import javax.swing.JOptionPane;

public class Urlaub {

	public static void main(String[] args) {

		String budgetEingabe = JOptionPane.showInputDialog("Wieviel beträgt Ihr Reisebudget?");
		double budget = Double.parseDouble(budgetEingabe);

		JOptionPane.showMessageDialog(null,
				"Für den Währungswechsel stehen folgende Wechselkurse, bezogen auf 1 EURO, zur Auswahl:\n\n"
						+ "USA: 1,22 USD (Dollar)\n" + "Japan: 126,50 JPY (Yen)\n" + "England: 0,89 GBP (Pfund)\n"
						+ "Schweiz: 1,08 CHF (Schweizer Franken)\n" + "Türkei: 12,13 YTL (Türkische Lira)");

		DecimalFormat f = new DecimalFormat("##.00");

		while (budget > 1.00) {
			String wechselEingabe = JOptionPane.showInputDialog("Wieviel Euro möchten Sie umtauschen?");
			double wechselBetrag = Double.parseDouble(wechselEingabe);

			String wechselkurs = JOptionPane.showInputDialog(
					"In welche Währung möchten Sie den Betrag: " + f.format(wechselBetrag) + " € umtauschen?");

			if (wechselkurs.equals("USD") | wechselkurs.equals("Dollar") | wechselkurs.equals("dollar")) {
				budget = Math.round(budget - wechselBetrag);
				JOptionPane.showMessageDialog(null,
						"Sie erhalten umgerechnet: " + f.format(umrechnungDollar(wechselBetrag)) + " $\n\n"
								+ "Sie verfügen noch: " + f.format(budget) + " € zum Umtauschen!");
			}
			if (wechselkurs.equals("JPY") | wechselkurs.equals("Yen") | wechselkurs.equals("yen")) {
				budget = Math.round(budget - wechselBetrag);
				JOptionPane.showMessageDialog(null,
						"Sie erhalten umgerechnet: " + f.format(umrechnungYen(wechselBetrag)) + " ¥\n\n"
								+ "Sie verfügen noch:" + f.format(budget) + " € zum Umtauschen!");
			}
			if (wechselkurs.equals("GBP") | wechselkurs.equals("pfund") | wechselkurs.equals("Pfund")) {
				budget = Math.round(budget - wechselBetrag);
				JOptionPane.showMessageDialog(null,
						"Sie erhalten umgerechnet: " + f.format(umrechnungPfund(wechselBetrag)) + " £\n\n"
								+ "Sie verfügen noch:" + f.format(budget) + " € zum Umtauschen!");
			}
			if (wechselkurs.equals("CHF") | wechselkurs.equals("Franken") | wechselkurs.equals("franken")
					| wechselkurs.equals("Schweizer Franken")) {
				budget = Math.round(budget - wechselBetrag);
				JOptionPane.showMessageDialog(null,
						"Sie erhalten umgerechnet: " + f.format(umrechnungFranken(wechselBetrag)) + " CHF\n\n"
								+ "Sie verfügen noch: " + f.format(budget) + " € zum Umtauschen!");
			}
			if (wechselkurs.equals("YTL") | wechselkurs.equals("TL") | wechselkurs.equals("Lira")
					| wechselkurs.equals("lira")) {
				budget = Math.round(budget - wechselBetrag);
				JOptionPane.showMessageDialog(null,
						"Sie erhalten umgerechnet: " + f.format(umrechnungLira(wechselBetrag)) + " ₺\n\n"
								+ "Sie verfügen noch: " + f.format(budget) + " € zum Umtauschen!");
			}
			// Bedingungen für mögliche Falscheingaben:
			if (wechselkurs == "" | !wechselkurs.equals("USD") | !wechselkurs.equals("Dollar")
					| !wechselkurs.equals("dollar") | !wechselkurs.equals("JPY") | !wechselkurs.equals("yen")
					| !wechselkurs.equals("Yen") | !wechselkurs.equals("GBP") | !wechselkurs.equals("Pfund")
					| !wechselkurs.equals("pfund") | !wechselkurs.equals("CHF") | !wechselkurs.equals("franken")
					| !wechselkurs.equals("Franken") | !wechselkurs.equals("Schweizer Franken")
					| !wechselkurs.equals("YTL") | !wechselkurs.equals("TL") | !wechselkurs.equals("Lira")
					| !wechselkurs.equals("lira")) {
				JOptionPane.showMessageDialog(null,
						"Ups!\n Anscheinend haben Sie eine falsche Eingabe getätigt.\n Bitte wiederholen Sie Ihre Eingabe.\n\n"
								+ "Klicken Sie hierzu bitte auf \"OK\".\n");
			} // Ende der Bedingungen für Falscheingaben.

			if (budget <= 0.00) {
				JOptionPane.showMessageDialog(null, "Sie haben kein Bargeld in Euro mehr zum umtauschen!\n"
						+ "Bitte fahren Sie nach Hause. Ihre Reise ist beendet!\n\n" + "Das Programm wird beendet.");
			}
		}
	}
	// Methoden zur Umrechnung in jew. Währungen

	public static double umrechnungDollar(double euro) {
		double dollarkurs = 1.22;
		double dollar = euro * dollarkurs;
		return dollar;
	}

	public static double umrechnungYen(double euro) {
		double yenKurs = 126.50;
		double yen = euro * yenKurs;
		return yen;
	}

	public static double umrechnungPfund(double euro) {
		double pfundKurs = 0.89;
		double pfund = euro * pfundKurs;
		return pfund;
	}

	public static double umrechnungFranken(double euro) {
		double frankenKurs = 1.08;
		double franken = euro * frankenKurs;
		return franken;
	}

	static double umrechnungLira(double euro) {
		double liraKurs = 12.13;
		double lira = euro * liraKurs;
		return lira;
	}
}
