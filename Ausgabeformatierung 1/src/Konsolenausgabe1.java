
public class Konsolenausgabe1 {

	public static void main(String[] args) {
		
		//Aufgabe 1:
		System.out.println("L�sung zur Aufgabe 1:");
		
		System.out.print("\nDas ist ein \"Beispiel\".");
		System.out.print("\nEin Beispiel ist das.\n\n");
		// Das ist ein Kommentar.
		
		//Aufgabe 2:
		String s = "**************";
		
		System.out.println("L�sung zur Aufgabe 2:");
		
		System.out.printf("\n%7.1s", s);
		System.out.printf("\n%8.3s", s);
		System.out.printf("\n%9.5s", s);
		System.out.printf("\n%10.7s", s);
		System.out.printf("\n%11.9s", s);
		System.out.printf("\n%12.11s", s);
		System.out.printf("\n%13.13s", s);
		System.out.printf("\n%8.3s", s);
		System.out.printf("\n%8.3s\n\n", s);
		
		//Aufgabe 3:
		System.out.println("L�sung zur Aufgabe 3");
		
		System.out.printf("\n%.2f\n", 22.4234234);
		System.out.printf("%.2f\n", 111.2222);
		System.out.printf("%.2f\n", 4.0);
		System.out.printf("%.2f\n", 1000000.551);
		System.out.printf("%.2f\n", 97.34);
		
	}

}
