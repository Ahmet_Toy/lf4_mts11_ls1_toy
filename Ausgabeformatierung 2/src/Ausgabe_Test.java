import java.util.Scanner;

public class Ausgabe_Test {

	public static void main(String[] args) {

		Scanner eingabe = new Scanner(System.in);

		System.out.println("Geben Sie einen Startwert f�r die Umrechnung in Fahrenheit ein: ");

		double startC = eingabe.nextDouble();

		System.out.println("Geben Sie einen Endwert f�r die Umrechnung in Fahrengeit an: ");
		double endeC = eingabe.nextDouble();
		
		System.out.println("In welchen Schritten sollen die Werte umgerechnet werden: ");
		double schritt = eingabe.nextDouble();
		
		eingabe.close();
		
		double umrechnungsfaktor = 1.8;
		
		System.out.printf("%1.2f", startC);

		while (startC <= endeC) {
			System.out.println(startC);
			startC = startC + schritt;
		}
	}
}