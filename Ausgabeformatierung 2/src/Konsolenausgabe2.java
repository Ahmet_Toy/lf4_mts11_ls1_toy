
public class Konsolenausgabe2 {

	public static void main(String[] args) {
		
		//Aufgabe 1:
		System.out.println("L�sung zur Aufgabe 1:");
		
		String s = "**";
		System.out.printf( "\n%6s\n", s );
		System.out.printf( "%2.1s", s );
		System.out.printf( "%7.1s\n", s );
		System.out.printf( "%2.1s", s );
		System.out.printf( "%7.1s\n", s );
		System.out.printf( "%6s\n", s );
		
		
		// Aufgabe 2:
		System.out.println("\nL�sung zur Aufgabe 2:");
		
		System.out.printf("\n%-5s %-19s %s %4s\n", "0!", "=", "=", "1");
		System.out.printf("%-5s %-19s %s %4s\n", "1!", "= 1", "=", "1");
		System.out.printf("%-5s %-19s %s %4s\n", "2!", "= 1 * 2", "=", "2");
		System.out.printf("%-5s %-19s %s %4s\n", "3!", "= 1 * 2 * 3", "=", "6");
		System.out.printf("%-5s %-19s %s %4s\n", "4!", "= 1 * 2 * 3 * 4", "=", "24");
		System.out.printf("%-5s %-19s %s %4s\n", "5!", "= 1 * 2 * 3 * 4 * 5", "=", "120");
		
		
		//Aufgabe 3:
		System.out.println("\nL�sung zur Aufgabe 3:");
		
		String trennlinie = "|";
		String bindestrich = "-----------------------";
		String plus = "+";
		
		System.out.printf("\n%-12s", "Fahrenheit");
		System.out.printf("%s", trennlinie);
		System.out.printf("%10s\n", "Celsius");
		
		System.out.printf("%s", bindestrich);
		
		System.out.printf("\n%-12d", -20);
		System.out.printf("%s", trennlinie);
		System.out.printf("%10.2f\n", -28.89);
		
		System.out.printf("%-12d", -10);
		System.out.printf("%s",trennlinie);
		System.out.printf("%10.2f\n", -23.33);
		
		System.out.printf("%s", plus);
		System.out.printf("%-11d", 0);
		System.out.printf("%s",trennlinie);
		System.out.printf("%10.2f\n", -17.78);
		
		System.out.printf("%s", plus);
		System.out.printf("%-11d", 20);
		System.out.printf("%s",trennlinie);
		System.out.printf("%10.2f\n", -6.67);
		
		System.out.printf("%s", plus);
		System.out.printf("%-11d", 30);
		System.out.printf("%s",trennlinie);
		System.out.printf("%10.2f\n", -1.11);		
	}
}
