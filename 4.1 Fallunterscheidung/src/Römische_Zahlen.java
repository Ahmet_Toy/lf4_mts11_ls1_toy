import java.util.Scanner;

public class R�mische_Zahlen {

	public static void main(String[] args) {

		Scanner eingabe = new Scanner(System.in);

		System.out.println("Bitte geben Sie eine r�mische Zahl ein: ");
		char roemischeZahl = eingabe.next().charAt(0);

		short dezimal = 0;

		switch (roemischeZahl) {

		case 'I':
			dezimal = 1;
			break;
		case 'V':
			dezimal = 5;
			break;
		case 'X':
			dezimal = 10;
			break;
		case 'L':
			dezimal = 50;
			break;
		case 'C':
			dezimal = 100;
			break;
		case 'D':
			dezimal = 500;
			break;
		case 'M':
			dezimal = 1000;
			break;
		default:
			System.out.println("Fehler! Geben Sie ein r�misches Zahlenzeichen ein: ");
			
		}
		System.out.println("Die r�mische Zahl " + roemischeZahl + " entspricht der Zahl " + dezimal + " im Dezimalsystem");
		eingabe.close();
	}
}
