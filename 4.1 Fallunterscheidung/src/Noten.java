import java.util.Scanner;

public class Noten {

	public static void main(String[] args) {

		Scanner eingabe = new Scanner(System.in);

		System.out.println("Geben Sie eine Note ein: ");

		int note = eingabe.nextInt();
		String definition = "";

		switch (note) {

		case 1:
			definition = "Sehr gut";
			break;
		case 2:
			definition = "Gut";
			break;
		case 3:
			definition = "Befriedigend";
			break;
		case 4:
			definition = "Ausreichend";
			break;
		case 5:
			definition = "Mangelhaft";
			break;
		case 6:
			definition = "Ungen�gend";
			break;
		default:
			System.out.println("Fehler! Geben Sie eine Note von 1 bis 6 ein");
			break;
		}
		if (note > 0 && note < 7) {
			System.out.printf("Die Note " + note + " steht f�r: " + definition);
		}
		eingabe.close();
	}
}
