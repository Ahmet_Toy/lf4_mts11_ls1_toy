import java.util.Scanner;

public class Monate {

	public static void main(String[] args) {

		Scanner eingabe = new Scanner(System.in);

		System.out.println("Geben Sie eine Zahl 1 - 12 f�r den Monat an: ");
		int zahl = eingabe.nextInt();
		String monat = "";

		switch (zahl) {

		case 1:
			monat = "Januar";
			break;
		case 2:
			monat = "Februar";
			break;
		case 3:
			monat = "M�rz";
			break;
		case 4:
			monat = "April";
			break;
		case 5:
			monat = "Mai";
			break;
		case 6:
			monat = "Juni";
			break;
		case 7:
			monat = "Juli";
			break;
		case 8:
			monat = "August";
			break;
		case 9:
			monat = "September";
			break;
		case 10:
			monat = "Oktober";
			break;
		case 11:
			monat = "November";
			break;
		case 12:
			monat = "Dezember";
			break;
		default:
			System.out.println("Fehler! Geben Sie eine Zahl 1 - 12 ein: ");
		}
		if (zahl > 0 && zahl < 13) {
			System.out.println("Der eingegebene Monat ist: " + monat);
		}
		eingabe.close();
	}

}
