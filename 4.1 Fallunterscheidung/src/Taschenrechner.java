import java.util.Scanner;

public class Taschenrechner {

	public static void main(String[] args) {

		Scanner eingabe = new Scanner(System.in);

		System.out.println("Eingabe Zahl 1: ");
		int zahl1 = eingabe.nextInt();

		System.out.println("Eingabe Zahl 2: ");
		int zahl2 = eingabe.nextInt();

		System.out.println("Eingabe Operator: ");
		char op = eingabe.next().charAt(0);

		int ergebnis = 0;

		switch (op) {

		case '+':
			ergebnis = zahl1 + zahl2;
			break;
		case '-':
			ergebnis = zahl1 - zahl2;
			break;
		case '*':
			ergebnis = zahl1 * zahl2;
			break;
		case '/':
			ergebnis = zahl1 / zahl2;
			break;
		default:
			System.out.println("Bitte geben Sie einen g�ltigen Operator ein");
		}
		System.out.println("Das Ergebnis der Berechnung ist: " + ergebnis);
		eingabe.close();
	}

}
