import javax.swing.JOptionPane;

public class Fahrkartenautomat_Array {

	static double[] tarifpreis = new double[10];

	public static void main(String[] args) {

		String[] tarifbezeichnung = new String[10];
		tarifbezeichnung[0] = "1.  Einzelfahrschein Berlin AB";
		tarifbezeichnung[1] = "2.  Einzelfahrschein Berlin AC";
		tarifbezeichnung[2] = "3.  Einzelfahrschein Berlin ABC";
		tarifbezeichnung[3] = "4.  Kurzstrecke\t\t";
		tarifbezeichnung[4] = "5.  Tageskarte Berlin AB\t";
		tarifbezeichnung[5] = "6.  Tageskarte Berlin AC\t";
		tarifbezeichnung[6] = "7.  Tageskarte Berlin ABC\t";
		tarifbezeichnung[7] = "8.  Kleingruppen-Tageskarte AB";
		tarifbezeichnung[8] = "9.  Kleingruppen-Tageskarte AC";
		tarifbezeichnung[9] = "10. Kleingruppen-Tageskarte ABC";

		tarifpreis[0] = 2.90;
		tarifpreis[1] = 3.30;
		tarifpreis[2] = 3.60;
		tarifpreis[3] = 1.90;
		tarifpreis[4] = 8.60;
		tarifpreis[5] = 9.00;
		tarifpreis[6] = 9.60;
		tarifpreis[7] = 23.50;
		tarifpreis[8] = 24.30;
		tarifpreis[9] = 24.90;

		System.out.println("Men�: \n");

		for (int i = 0; i <= tarifbezeichnung.length; i++) {
			System.out.print(tarifbezeichnung[i] + " \t");
			System.out.printf("%10.2f", tarifpreis[i]);
			System.out.println(" �");
		}

		boolean neuerFahrschein = true;

		while (neuerFahrschein = true) {

			String fahrkartenAuswahl = JOptionPane.showInputDialog("Bitte w�hlen Sie die Nummer f�r eine Fahrkarte: ");
			int tarif = Integer.parseInt(fahrkartenAuswahl) - 1;

			if (tarif < 0 && tarif > 9) {
				JOptionPane.showInputDialog("Fehler! Bitte geben Sie eine g�ltige Nummer ein: ");
				tarif = Integer.parseInt(fahrkartenAuswahl) - 1;
			}
			String anzahlTickets = JOptionPane.showInputDialog("Wieviele Tickets von: " + tarifbezeichnung[tarif]
					+ "m�chten Sie kaufen?" + " \n Maximale Anzahl: 5");
			int anzahlKauf = Integer.parseInt(anzahlTickets);

			if (anzahlKauf < 1 && anzahlKauf > 5) {
				anzahlTickets = JOptionPane
						.showInputDialog("Fehler! Bitte geben Sie eine Anzahl zwischen 1 und 5 an: ");
				anzahlKauf = Integer.parseInt(anzahlTickets);
			}
			double zuZahlenderBetrag = fahrkartenbestellungErfassen(tarif, anzahlKauf);
			JOptionPane.showMessageDialog(null, "Gesamtbetrag zu zahlen: " + zuZahlenderBetrag + " �");

			/*
			 * Geldeinwurf
			 */
			double rueckgabe = fahrkarteBezahlen(zuZahlenderBetrag);

			/*
			 * Fahrscheinausgabe
			 */
			JOptionPane.showMessageDialog(null,
					"Vielen Dank f�r Ihren Kauf!\n" + "Bitte entnehmen Sie Ihren Fahrschein");

			/*
			 * Ausgabe R�ckgeld
			 */
			if (rueckgabe > 0.0) {
				JOptionPane.showMessageDialog(null,
						"Ihr Wechselgeld betr�gt: " + rueckgabe + " �"
								+ "\n\n Vergessen Sie nicht, den Fahrschein vor Fahrtantritt entwerten zu lassen!"
								+ "\n Wir w�nschen Ihnen eine gute Fahrt.");
			}
		}
	}

	/*
	 * Methoden
	 */

	static double fahrkartenbestellungErfassen(int tarif, int anzahl) {
		double ergebnis;
		ergebnis = tarifpreis[tarif] * anzahl;
		return ergebnis;
	}

	static double fahrkarteBezahlen(double zuZahlen) {
		double eingezahlt = 0.0;

		while (eingezahlt < zuZahlen) {
			String einwurf = JOptionPane.showInputDialog("Noch zu Zahlen: " + zuZahlen);
			eingezahlt = eingezahlt + Double.parseDouble(einwurf);
		}
		return eingezahlt;
	}
}