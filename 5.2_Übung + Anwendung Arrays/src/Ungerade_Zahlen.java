
public class Ungerade_Zahlen {

	public static void main(String[] args) {

		int[] ungeradeZahlen = new int[20];

		for (int i = 1; i < ungeradeZahlen.length; i++) {
			if (i % 2 == 1) {
				ungeradeZahlen[i] = i;
				System.out.print(ungeradeZahlen[i] + ", ");
			}
		}
	}
}
