import java.util.Scanner;

public class Fahrkartenautomat_Endlosmodus {

	public static void main(String[] args) {

		byte tarifwahl;
		byte anzahlFahrkarten;
		double ticketPreis = 0, zuZahlenderBetrag, rueckgabebetrag;

		// Tarifpreise:
		double tarif1 = 2.90;
		double tarif2 = 8.60;
		double tarif3 = 23.50;

		System.out.println("W�hlen Sie einen Tarif f�r Berlin AB aus");
		System.out.println("Bitte geben Sie eine der folgenden Tarif-Nummer an: \n\n");
		System.out.println("Tarif 1:\t Einzelfahrt \t 2,90�");
		System.out.println("Tarif 2:\t Tageskarte \t 8,60");
		System.out.println("Tarif 3:\t Wochenkarte \t 23,50");

		boolean neuerFahrschein = true;

		while (neuerFahrschein = true) {

			Scanner eingabe = new Scanner(System.in);

			System.out.println("\nW�hlen Sie Ihre Tarif-Nummer: ");
			tarifwahl = eingabe.nextByte();

			while (tarifwahl < 1 && tarifwahl > 3) {
				System.out.println("Falsche Eingabe! Bitte geben Sie erneut eine Tarif-Nummer ein: ");
				tarifwahl = eingabe.nextByte();
			}
			if (tarifwahl == 1) {
				ticketPreis = tarif1;
				System.out.println("Ihre Wahl: " + tarifwahl + "\n");
			}
			if (tarifwahl == 2) {
				ticketPreis = tarif2;
				System.out.println("Ihre Wahl: " + tarifwahl + "\n");
			}
			if (tarifwahl == 3) {
				ticketPreis = tarif3;
				System.out.println("Ihre Wahl: " + tarifwahl + "\n");
			}

			System.out.print("Anzahl der Fahrkarten: ");
			anzahlFahrkarten = eingabe.nextByte();

			if (anzahlFahrkarten < 1 && anzahlFahrkarten > 10) {
				anzahlFahrkarten = 1;
				System.out.println("Hinweis: Ung�ltige Angabe. Die Anzahl wurde automatisch auf 1 gesetzt.");
			}

			System.out.print("Zu zahlender Betrag (EURO): ");
			System.out.printf("%4.2f\n",
					zuZahlenderBetrag = fahrkartenbestellungErfassen(anzahlFahrkarten, ticketPreis));

			// Geldeinwurf
			// -----------
			rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);

			// Fahrscheinausgabe
			// -----------------
			System.out.println("\nFahrschein wird ausgegeben");
			for (int i = 0; i < 8; i++) {

				System.out.print("=");
				try {
					Thread.sleep(250);
				} catch (InterruptedException e) {

					e.printStackTrace();
				}
			}
			System.out.println("\n\n");

			// R�ckgeldberechnung und -Ausgabe
			// -------------------------------

			if (rueckgabebetrag > 0.0) {

				System.out.print("Der R�ckgabebetrag in H�he von ");
				System.out.printf("%4.2f", rueckgabebetrag);
				System.out.print(" Euro");
				System.out.println(" wird in folgenden M�nzen ausgezahlt:");

				while (rueckgabebetrag >= 2.0) { // 2 EURO-M�nzen

					System.out.println("2 EURO");
					rueckgabebetrag -= 2.0;
				}
				while (rueckgabebetrag >= 1.0) { // 1 EURO-M�nzen

					System.out.println("1 EURO");
					rueckgabebetrag -= 1.0;
				}
				while (rueckgabebetrag >= 0.5) { // 50 CENT-M�nzen

					System.out.println("50 CENT");
					rueckgabebetrag -= 0.5;
				}
				while (rueckgabebetrag >= 0.2) { // 20 CENT-M�nzen

					System.out.println("20 CENT");
					rueckgabebetrag -= 0.2;
				}
				while (rueckgabebetrag >= 0.1) { // 10 CENT-M�nzen

					System.out.println("10 CENT");
					rueckgabebetrag -= 0.1;
				}
				while (rueckgabebetrag >= 0.05) { // 5 CENT-M�nzen

					System.out.println("5 CENT");
					rueckgabebetrag -= 0.05;
				}
			}

			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir w�nschen Ihnen eine gute Fahrt.");

			System.out.println("\nBitte warten. Die Eingabe-Seite wird neu geladen");
			for (int i = 0; i < 8; i++) {

				System.out.print("...");
				try {
					Thread.sleep(250);
				} catch (InterruptedException e) {

					e.printStackTrace();
				}
			}
			System.out.println("\n");
			eingabe.close();
		} // Ende der While-Schleife
	}// Ende der Main-Methode

	// Methoden -----------------------------------------------------------

	static double fahrkartenbestellungErfassen(double anzahl, double preis) {
		double ergebnis;
		if (anzahl < 0 && anzahl > 10) {
			anzahl = 1;
		}
		ergebnis = anzahl * preis;
		return ergebnis;
	}

	static double fahrkartenBezahlen(double zuZahlen) {
		Scanner tastatur = new Scanner(System.in);
		double eingezahlt = 0.0;
		while (eingezahlt < zuZahlen) {

			System.out.print("Noch zu zahlen: ");
			System.out.printf("%4.2f", zuZahlen - eingezahlt);
			System.out.println(" Euro");
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			double einwurf = tastatur.nextDouble();
			eingezahlt += einwurf;
		}
		tastatur.close();
		double rueckgeld = eingezahlt - zuZahlen;
		return rueckgeld;
	}

}