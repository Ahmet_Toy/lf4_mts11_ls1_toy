
public class Welt_der_Zahlen {

	public static void main(String[] args) {
		/**
		  *   Aufgabe:  Recherchieren Sie im Internet !
		  * 
		  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
		  *
		  *   Vergessen Sie nicht den richtigen Datentyp !!
		  *
		  *
		  * @version 1.0 from 21.08.2019
		  * @author << Ahmet Toy >>
		  */

		    
		    /*  *********************************************************
		    
		         Zuerst werden die Variablen mit den Werten festgelegt!
		    
		    *********************************************************** */
		    // Im Internet gefunden ?
		    // Die Anzahl der Planeten in unserem Sonnesystem                    
		    int anzahlPlaneten = 8  ;
		    
		    // Anzahl der Sterne in unserer Milchstra�e
		    long anzahlSterne = 150000000000L;
		    
		    // Wie viele Einwohner hat Berlin?
		    int bewohnerBerlin = 3766082;
		    
		    // Wie alt bist du?  Wie viele Tage sind das?
		    
		    short alterTage = 10220;
		    
		    // Wie viel wiegt das schwerste Tier der Welt?
		    // Schreiben Sie das Gewicht in Kilogramm auf!
		    int gewichtKilogramm = 150000;  
		    
		    // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
		     int flaecheGroessteLand = 17130000;
		    
		    // Wie gro� ist das kleinste Land der Erde?
		    
		     double flaecheKleinsteLand = 0.44;
		    
		    
		    
		    
		    /*  *********************************************************
		    
		         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
		    
		    *********************************************************** */
		    
		    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
		    System.out.println("Anzahl der Sterne: " + anzahlSterne);
		    System.out.println("Bev�lkerung von Berlin: " + bewohnerBerlin);
		    System.out.println("Mein Alter in Tagen: " + alterTage);
		    System.out.println("Gewicht des schwersten Tieres der Welt: " + gewichtKilogramm + " kg");
		    System.out.println("Fl�che des gr��ten Landes: " + flaecheGroessteLand + " m�");
		    System.out.println("Fl�che des kleinsten Landes: " + flaecheKleinsteLand + " m�");
		    
		    
		    
		    
		    System.out.println("\n *******  Ende des Programms  ******* ");
		    
	}		
}


