import java.util.Scanner;

public class Temperaturumrechnung {

	public static void main(String[] args) {

		Scanner eingabe = new Scanner(System.in);

		System.out
				.println("Bitte geben Sie folgende Werte ein f�r die Umrechnung von Grad Celsius nach Fahrenheit: \n");

		System.out.println("Bitte den Startwert in Grad Celsius eingeben: ");
		double celsiusStart = eingabe.nextDouble();

		System.out.println("Bitte den Endwert in Grad Celsius eingeben: ");
		double celsiusEnde = eingabe.nextDouble();

		System.out.println("Bitte die Schrittweite in Grad Celsius eingeben: \n");
		double schrittweite = eingabe.nextDouble();
		double fahrenheit;
		double umrechnungsfaktor = 1.8;

		while (celsiusStart <= celsiusEnde) {
			fahrenheit = celsiusStart * umrechnungsfaktor + 32;
			System.out.print(celsiusStart + "�C\t\t");
			System.out.println(fahrenheit + " F");
			celsiusStart = celsiusStart + schrittweite;
		}
		eingabe.close();
	}

}
