import java.util.Scanner;

public class Quersumme {

	public static void main(String[] args) {

		Scanner eingabe = new Scanner(System.in);

		System.out.println("Geben Sie eine Zahl ein, f�r die die Quersumme gebildet werden soll: ");
		int zahl = eingabe.nextInt();
		
		final int uebergebeneZahl = zahl;
		int summe = 0;

		while (zahl != 0) {
			summe = summe + (zahl % 10);
			// durch mod 10 erh�lt man immer die letzte Ziffer einer Zahl, die dann zur
			// Summe addiert werden kann
			zahl = zahl / 10;
			// durch das teilen mit 10 wird die letzte Ziffer der Zahl eliminiert
		}
		System.out.println("Die Quersumme der eingegebenen Zahl: " + uebergebeneZahl + " ist = " + summe);
		eingabe.close();
	}	

}
