import java.util.Scanner;

public class Zaehlen {

	public static void main(String[] args) {

		Scanner eingabe = new Scanner(System.in);

		// a)
		System.out.println("Bis zu welcher Zahl soll heraufgez�hlt werden? ");
		int n = eingabe.nextInt();

		int i = 1;

		while (i <= n) {
			System.out.print(i + " ");
			i++;
		}
		System.out.println("\n");
		// b)
		System.out.println("Ab welcher Zahl soll herabgez�hlt werden? ");
		int z = eingabe.nextInt();

		while (z >= 0) {
			System.out.print(z + " ");
			z--;
		}
		eingabe.close();
	}
}
