import java.util.Scanner;
public class Aufgabe_2 {

	public static void main(String[] args) {
		
		// Aufgabe 2:
		
		Scanner eingabe = new Scanner(System.in);

		System.out.println("\nBitte geben Sie den Nettowert der Rechnung an");
		double nettowert = eingabe.nextDouble();

		System.out.println("Bitte geben Sie (j) f�r den erm��igten Steuersatz und (n) f�r den vollen Steursatz an: ");
		String steuersatz = eingabe.next();

		double ermSteuersatz = 0.7;
		double vollSteuersatz = 0.19;

		char s = steuersatz.charAt(0);
		double steuerBetrag;

		if (s == 'j') {
			steuerBetrag = nettowert * ermSteuersatz;
		} else {
			steuerBetrag = nettowert * vollSteuersatz;
		}
		double bruttowert = nettowert + steuerBetrag;

		System.out.print("Der Brutto-Rechnungsbetrag ist: � ");
		System.out.printf("%.2f\n", bruttowert);
		eingabe.close();
	}

}
