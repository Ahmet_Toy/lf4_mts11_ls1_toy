import java.util.Scanner;

public class Aufgabe_3 {

	public static void main(String[] args) {
		// Aufgabe 3:
		
		Scanner eingabe = new Scanner(System.in);

		double preisMaus = 19.95;
		double preisLieferung = 5.90;
		double rechnung;
		System.out.print("Preis f�r eine Maus: ");
		System.out.printf("%.2f\n", preisMaus);
		
		System.out.print("Die Lieferkosten betragen: ");
		System.out.printf("%.2f\n", preisLieferung);
		
		System.out.println("Kostenfreie Lieferung bei Bestellung ab 10 M�usen!");
		System.out.println("Bestellmenge M�use: ");
		int anzahlMaus = eingabe.nextInt();

		if (anzahlMaus >= 10) {
			rechnung = anzahlMaus * preisMaus;
		} else {
			rechnung = anzahlMaus * preisMaus + preisLieferung;
		}
		
		System.out.print("Rechnungsbetrag: ");
		System.out.printf("%.2f\n", rechnung);
		eingabe.close();
	}
}


