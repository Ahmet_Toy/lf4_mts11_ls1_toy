import java.util.Scanner;

public class Aufgabe_1 {

	public static void main(String[] args) {

		// Aufgabe 1: Eigene Bedingungen

		Scanner myScanner = new Scanner(System.in);

		System.out.println("Bitte gebe an, auf wieviel Uhr dein Wecker gestellt ist: ");

		int uhrzeit1 = myScanner.nextInt();

		System.out.println("Bitte gebe an, um wieviel Uhr die Schule beginnt: ");
		int uhrzeit2 = myScanner.nextInt();

		if (uhrzeit1 == 7) {
			System.out.println("Aufwachen und auf den Weg machen");
		}

		// Aufgabe 1.2

		if (uhrzeit1 == uhrzeit2) {
			System.out.println("Du muss deinen Wecker fr�her stellen");
		}

		// Aufgabe 1.3

		if (uhrzeit1 < uhrzeit2) {
			System.out.println("Du bist noch nicht zu sp�t aber beeil dich!");
		}

		// Aufgabe 1.4

		if (uhrzeit1 >= uhrzeit2) {
			System.out.println("Du kommst zu sp�t");
		} else {
			System.out.printf("Du bist noch nicht zu sp�t\n");
		}
		myScanner.close();
	}
}
