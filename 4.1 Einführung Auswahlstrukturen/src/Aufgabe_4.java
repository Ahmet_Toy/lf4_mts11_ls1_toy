import java.util.Scanner;

public class Aufgabe_4 {

	public static void main(String[] args) {

		Scanner eingabe = new Scanner(System.in);

		System.out.println("Wie hoch ist Ihr Bestellwert? ");
		double bestellwertNetto = eingabe.nextDouble();
		double rabatt;

		if (bestellwertNetto <= 100.00) {
			rabatt = bestellwertNetto * 0.10;
		} else if (bestellwertNetto > 100.00 && bestellwertNetto <= 500.00) {
			rabatt = bestellwertNetto * 0.15;
		} else {
			rabatt = bestellwertNetto * 0.20;
		}
		System.out.print("Ihr Rabatt betr�gt: ");
		System.out.printf("%.2f", rabatt);
		System.out.println(" �");
		double steuerBetrag = 1.19;
		double bestellwertBrutto = (bestellwertNetto - rabatt) * steuerBetrag;
		System.out.print("Der erm��igte Bestellwert inkl. MwSt. betr�gt: ");
		System.out.printf("%.2f", bestellwertBrutto);
		System.out.println(" �");
		eingabe.close();
	}
}