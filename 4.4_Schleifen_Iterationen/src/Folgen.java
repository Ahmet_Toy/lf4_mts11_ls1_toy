
public class Folgen {

	public static void main(String[] args) {

		// a)
		System.out.print("a) ");
		for (int i = 99; i >= 0; i = i - 3) {
			System.out.print(i + " ");
		}
		System.out.println(" ");

		// b)
		System.out.print("b) ");
		for (int i = 1; i <= 400; i++) {
			System.out.print((i * i) + " ");
		}
		System.out.println(" ");

		// c)
		System.out.print("c) ");
		for (int i = 2; i <= 102; i = i + 4) {
			System.out.print(i + " ");
		}
		System.out.println("");

		// d)
		System.out.print("d) ");
		int plus = 4;
		for (int i = 4; i <= 1024; i = i + plus) {
			System.out.print(i + " ");
			plus = plus +8;
		}
		System.out.println("");
		
		// e)
		System.out.print("e) ");
		int verdopplung = 1;
		for (int i =2; i<=32768; i = i + verdopplung) {
			System.out.print(i + " ");
			verdopplung = verdopplung * 2;
		}
	}

}
