import java.util.Scanner;

public class Zaehlen {

	public static void main(String[] args) {
		
		//Aufgabe 1 a:
		
		Scanner eingabe = new Scanner(System.in);
		
		System.out.println("Bis zu welcher Zahl soll heraufgez�hlt werden? ");
		
		int n = eingabe.nextInt();
		
		int i;
		
		for (i=1; i<=n; i++) {
			System.out.println(i);
		}
		//Aufgabe 1 b:
		
		System.out.println("Von welcher Zahl soll heruntergez�hlt werden? ");
		
		int n2 = eingabe.nextInt();
		
		int j;
		
		for (j=n2; j>=1; j--) {
			System.out.println(j);
		}
		eingabe.close();
	}
}
