import java.util.ArrayList;

public class Modulo {

	public static void main(String[] args) {

		ArrayList<Integer> sieben = new ArrayList<>();

		for (int i = 1; i <= 200; i++) {
			if (i % 7 == 0) {
				sieben.add(i);
			}
		}
		System.out.println("Durch 7 teilbare Zahlen: ");
		for (int i: sieben) {
			System.out.print(i + " ");
		}
	}

}
