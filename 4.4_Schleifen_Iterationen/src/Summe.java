import java.util.Scanner;

public class Summe {

	public static void main(String[] args) {

		Scanner eingabe = new Scanner(System.in);

		System.out.println("Bis zu welcher Zahl soll summiert werden? ");
		int n = eingabe.nextInt();

		int summe = 0;

		for (int i = n; 0 < i; i--) {
			// zwSumme = summe+i;
			summe = summe + i;
			
		}
		System.out.println("Summe: " + summe);
		eingabe.close();
	}
}
