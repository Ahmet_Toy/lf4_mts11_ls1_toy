
import java.util.Scanner;	// Import dieser Klasse ermöglicht Tastatureingaben

public class Konsoleneingabe {	//

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Guten Tag, herzlich Willkommen im Ticketsystem!\n");
		
		System.out.print("Bitte geben Sie Ihren Namen ein: ");
		String eingabeName = myScanner.next();
		
		System.out.print("Bitte geben Sie Ihr Alter an: ");
		int eingabeAlter = myScanner.nextInt();
		
		System.out.println("\nIhre angegebener Name lautet: " + eingabeName);
		System.out.println("\nIhr angegebenes Alter ist: " + eingabeAlter);
		
		myScanner.close();
		
		System.out.printf("\n%25s", "***Ende des Programms***");
	}

}
