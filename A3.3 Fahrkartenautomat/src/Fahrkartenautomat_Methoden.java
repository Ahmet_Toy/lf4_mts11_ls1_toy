import java.util.Scanner;

public class Fahrkartenautomat_Methoden {

	public static void main(String[] args) {

		Scanner tastatur = new Scanner(System.in);

		byte anzahlFahrkarten;
		double ticketPreis, zuZahlenderBetrag, eingezahlterGesamtbetrag, eingeworfeneMuenze, rueckgabebetrag;

		System.out.print("Ticketpreis: ");
		ticketPreis = tastatur.nextDouble();

		if (ticketPreis < 0 && ticketPreis > 10) {
			ticketPreis = 1;
			System.out.println("Hinweis: Ung�ltiger Ticketpreis. Der Ticketpreis wurde autom. auf 1,00 � gesetzt");
		}

		System.out.print("Anzahl der Fahrkarten: ");
		anzahlFahrkarten = tastatur.nextByte();

		/*if (anzahlFahrkarten < 1 && anzahlFahrkarten > 10) {
			anzahlFahrkarten = 1;
			System.out.println("Hinweis: Ung�ltige Anzahl. Die Anzahl der Fahrkarten wurden automatisch auf 1 gesetzt");
		}*/

		System.out.print("Zu zahlender Betrag (EURO): ");
		System.out.printf("%4.2f\n", zuZahlenderBetrag = fahrkartenbestellungErfassen(anzahlFahrkarten, ticketPreis));

		// Geldeinwurf
		// -----------
		rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);

		// Fahrscheinausgabe
		// -----------------
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {

			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}
		}
		System.out.println("\n\n");

		// R�ckgeldberechnung und -Ausgabe
		// -------------------------------

		if (rueckgabebetrag > 0.0) {

			System.out.print("Der R�ckgabebetrag in H�he von ");
			System.out.printf("%4.2f", rueckgabebetrag);
			System.out.print(" Euro");
			System.out.println(" wird in folgenden M�nzen ausgezahlt:");

			while (rueckgabebetrag >= 2.0) { // 2 EURO-M�nzen

				System.out.println("2 EURO");
				rueckgabebetrag -= 2.0;
			}
			while (rueckgabebetrag >= 1.0) { // 1 EURO-M�nzen

				System.out.println("1 EURO");
				rueckgabebetrag -= 1.0;
			}
			while (rueckgabebetrag >= 0.5) { // 50 CENT-M�nzen

				System.out.println("50 CENT");
				rueckgabebetrag -= 0.5;
			}
			while (rueckgabebetrag >= 0.2) { // 20 CENT-M�nzen

				System.out.println("20 CENT");
				rueckgabebetrag -= 0.2;
			}
			while (rueckgabebetrag >= 0.1) { // 10 CENT-M�nzen

				System.out.println("10 CENT");
				rueckgabebetrag -= 0.1;
			}
			while (rueckgabebetrag >= 0.05) { // 5 CENT-M�nzen

				System.out.println("5 CENT");
				rueckgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir w�nschen Ihnen eine gute Fahrt.");

	}
	// Methoden -----------------------------------------------------------

	static double fahrkartenbestellungErfassen(double anzahl, double preis) {
		double ergebnis;
		if (anzahl<0&&anzahl>10) {
			anzahl=1;
		}
		ergebnis = anzahl * preis;
		return ergebnis;
	}

	static double fahrkartenBezahlen(double zuZahlen) {
		Scanner tastatur = new Scanner(System.in);
		double eingezahlt = 0.0;
		while (eingezahlt < zuZahlen) {

			System.out.print("Noch zu zahlen: ");
			System.out.printf("%4.2f", zuZahlen - eingezahlt);
			System.out.println(" Euro");
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			double einwurf = tastatur.nextDouble();
			eingezahlt += einwurf;
		}
		tastatur.close();
		double rueckgeld = eingezahlt - zuZahlen;
		return rueckgeld;
	}

}