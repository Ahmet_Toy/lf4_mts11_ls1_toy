﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args) {
    
       Scanner tastatur = new Scanner(System.in);
      
       double 	ticketPreis;
       byte 	anzahlFahrkarten;
       double 	zuZahlenderBetrag;
       double 	eingezahlterGesamtbetrag;
       double 	eingeworfeneMuenze;
       double 	rueckgabebetrag;

       System.out.print("Ticketpreis (EURO): ");
       ticketPreis = tastatur.nextDouble();
       
       System.out.print("Anzahl der Fahrkarten: ");
       anzahlFahrkarten = tastatur.nextByte();
       
       zuZahlenderBetrag = ticketPreis * anzahlFahrkarten;						// Anzhahl Fahrkarten mit Einzelpreis multiplizieren
       System.out.print("Zu zahlender Betrag (EURO): ");
       System.out.printf("%4.2f\n", zuZahlenderBetrag);
       
       // Erläuterungen zu A2.5 Teilaufgabe 5 und 6 siehe unten am Programmende

       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag) {
       
    	   System.out.print("Noch zu zahlen: ");
    	   System.out.printf("%4.2f", zuZahlenderBetrag - eingezahlterGesamtbetrag);
    	   System.out.println(" Euro");
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingeworfeneMuenze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMuenze;
       }
       tastatur.close();

       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++) {
    	  
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(rueckgabebetrag > 0.0) {
       
    	   System.out.print("Der Rückgabebetrag in Höhe von ");
    	   System.out.printf("%4.2f", rueckgabebetrag);
    	   System.out.print(" Euro");
    	   System.out.println(" wird in folgenden Münzen ausgezahlt:");

           while(rueckgabebetrag >= 2.0) {	// 2 EURO-Münzen
           
        	  System.out.println("2 EURO");
	          rueckgabebetrag -= 2.0;
           }
           while(rueckgabebetrag >= 1.0) {	// 1 EURO-Münzen
           
        	  System.out.println("1 EURO");
	          rueckgabebetrag -= 1.0;
           }
           while(rueckgabebetrag >= 0.5) {	// 50 CENT-Münzen
           
        	  System.out.println("50 CENT");
	          rueckgabebetrag -= 0.5;
           }
           while(rueckgabebetrag >= 0.2) {	// 20 CENT-Münzen
           
        	  System.out.println("20 CENT");
 	          rueckgabebetrag -= 0.2;
           }
           while(rueckgabebetrag >= 0.1) {	// 10 CENT-Münzen
           
        	  System.out.println("10 CENT");
	          rueckgabebetrag -= 0.1;
           }
           while(rueckgabebetrag >= 0.05) {	// 5 CENT-Münzen
           
        	  System.out.println("5 CENT");
 	          rueckgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
       
    }
}
/* A 2.5 Teilaufgabe 5: Für die Eingabe des Ticketpreises habe ich ein Typ Double gewählt,
 * da der Ticketprteis auch Centbeträge haben kann. Für die Anzahl der Tickets habe ich den
 * Typ Byte gewählt, da meiner Meinung nach der Wertebereich bis 127 hier ausreichend ist
 * 
 *  Teilaufgabe 6: Bei der Berechnung anzahlFahrkaren * ticketPreis werden die jeweiligen 
 *  Werte, die mit der Scanner-Methode eingegeben wurden, multipliziert und das Ergebnis der
 *  Variable zuZahlenderBetrag zugewiesen */

